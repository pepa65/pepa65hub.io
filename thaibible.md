---
title: Thai Bible translations
description: This page is about different Thai Bible translations
keywords: Bible, Translation, Thai
permalink: /thaibible
---
[Karl Dahlfred]: //www.dahlfred.com
[A Brief Survey of Thai Bible Translations]: //www.dahlfred.com/en/blogs/gleanings-from-the-field/256-a-brief-survey-of-thai-bible-translations
[Wikipedia page on Thai Bible translations]: //en.wikipedia.org/wiki/Bible_translations_into_Thai
[please be in touch]: mailto:peter@passchier.net?subject=Thai%20Bible%20translations%20wiki "email"
[ABMU]: //en.wikipedia.org/wiki/American_Baptist_Missionary_Union
[Thailand Bible Society]: //www.thaibible.or.th/home/
[TheWord]: //www.theword.net
[Wordmodules]: //www.wordmodules.com
[YouVersion]: //www.bible.com "Bible app"
[Faith Comes by Hearing]: //www.faithcomesbyhearing.com
[iChurch]: //www.ichurch.in.th/bible
[Crosswire modules]: //www.crosswire.org/sword/modules
[The Internet Archive]: //www.archive.org/details/ThaiBible
[Biblica TNCV]: //www.biblica.com/bible/online-bible/tncv/
[Biblica TNCV NIV]: //www.biblica.com/bible/online-bible/tncv/%e0%b8%9b%e0%b8%90%e0%b8%a1%e0%b8%81%e0%b8%b2%e0%b8%a5/1/niv/
[Biblica]: //www.biblica.com
[International Bible League]: //www.bibleleague.org
[YWAM]: //www.ywam.org/
[Kanok Bannasaan]: //www.kanokbannasan.org "Publishing house"
[Philip Pope]: //www.thaipope.org
[Issuu ThCB]: //issuu.com/bosconoom/docs/new_testament_bible_mathew-act/139?e=0
[MySword]: //www.mysword.info/
[The Lord's Recovery in Thailand]: //lordsrecoveryinthailand.org
[termsook.com]: //termsook.com/AudioRCVBible.html
[Banpote Wetchgama]: //newbuddhists.wordpress.com/ "on Wordpress"
[Peter Passchier]: mailto:peter@passchier.net "email"


# [Thai Bible Translations](.)

I've found it very hard to find centralized information on Bible translations into the Thai language. [Karl Dahlfred][]'s article [A Brief Survey of Thai Bible Translations][] prompted me to summarize my initial findings on Thai Bible translations. There is now a [Wikipedia page on Thai Bible translations][] page, but it's not as comprehensive.

_**If you have any corrections or additional information, [please be in touch][]!**_

## Thai Bible 1894
John Taylor Jones, an [ABMU][] missionary, translated the New Testament into Thai language from Greek. Part of the Bible in Thai was first published in 1834, the New Testament in Thai was printed for the first time in 1843. The Old Testament was finished in 1883, and the first full collection of Bible texts in Thai came out in 1894.

## Thai Standard Version 1940 _([Thailand Bible Society][], 1940)_

A revision of the 1894 Thai Bible against the KJV. It is available as a module for the Bible software [TheWord][] from the Thai Bible Society. It can be found at [Wordmodules][]

**Mark 1:4** - ยอห์นให้เขารับบัพติศมาในถิ่นทุรกันดาร และประกาศเรื่องบัพติศมาอันสำแดงการกลับใจใหม่ เพื่อการยกโทษความผิดบาป

## Thai Holy Bible _([Thailand Bible Society][], 1971)_
**Mark 1:4** - ท่านยอห์นผู้ให้รับบัพติศมา ก็ได้ปรากฏตัวในถิ่นทุรกันดาร ท่านได้ประกาศให้กลับใจเสียใหม่ และรับบัพติศมา เพื่อพระเจ้าจะทรงยกความผิดบาปเสีย

Also called: ฉบับ มาตรฐาน, ฉบับ 1971, Thai Standard Version 1971,TSV, TH1971 (available on [YouVersion][]).
This is a revision of the 1940 version using the Revised Standard Version (RSV) and the Authorized Standard Version (ASV) as the base text.
This translation is currently used the most in Thai churches. Literal translation principles (it is following the English RSV), high language.
Audio of New Testament can be downloaded on [Faith Comes by Hearing][] (the _Non-Drama_ version)
Now online at [iChurch][]. [Crosswire modules][]: Thai1971, on [YouVersion][]: TH1971

## THSV Thai Standard Version _([Thailand Bible Society][], 2011 - earlier 2002/2006/2009)_
**Mark 1:4** - ยอห์นผู้ให้บัพติศมา ปรากฏตัวขึ้นในถิ่นทุรกันดาร ท่านประกาศถึงบัพติศมาที่แสดงการกลับใจใหม่ เพื่อรับการยกโทษความผิดบาป

Also called: ฉบับ มาตรฐาน 2011, THSV 2011, THSV (available on [YouVersion][]).
A revision of the 1971 edition.
Audio of New Testament can be downloaded (as a whole) from [Faith Comes by Hearing][] (the _Drama_ version). Each chapter can be downloaded as an mp3 from [The Internet Archive][]. [Crosswire modules][]: ThaiTSV, on [YouVersion][]: THSV2011, has full audio.

## TCL Thai Common Language version _([Thailand Bible Society][], 1985)_
**Mark 1:4** - ด้วย​เหตุนี้​เอง ยอห์น​ผู้​ประ‍กอบ​พิธี​ใช้​น้ำ​เข้า​จารีต จึง​เข้า​ไป​ใน​ถิ่น​กันดาร​เทศนา​ให้​ประ‍ชาชน​ฟัง​ว่า "จง​กลับ​ใจ​เสีย​ใหม่​และ​มา​รับ​พิธี​ใช้​น้ำ​เข้า​จารีต​เถิด แล้ว​พระ‍เจ้า​จะ​ทรง​อภัย​โทษ​บาป​ให้​ท่าน"

Also called: Prachaniyom, ประชานิยม
Only New Testament, Old Testament being developed. Copyright TBS 2002. [Crosswire modules][]: ThaiCL

## TNCV Thai New Contemporary Version _(Biblica, 2007)_
**Mark 1:4** - ยอห์นจึงมาให้บัพติศมาในถิ่นทุรกันดารและเทศนาให้กลับใจเสียใหม่และรับบัพติศมาเพื่อรับการอภัยโทษบาป

Also called: TNCV (online available on [YouVersion][]), ฉบับ อมตธรรมร่วมสมัย.
The TNCV is now downloadable as pdf's at [Biblica TNCV][]!
Read it online (parallel with NIV) at [Biblica TNCV NIV][].
The NIV used as a base for translation; this version is easier to read but still good for study. Used in student work.
There is also a 1999 edition (when the New Testament was completed); the audio has been recorded at Lopburi Language Centre by khruu Toye (called: the NIV version). The current TNCV is different. ([Biblica][] used to be the International Bible Society, then was called IBS-STL.) 
There is also a Thai Living Bible (1978?) that IBS/Biblica was involved with before the TNCV, also called ฉบับ อมตธรรมเพื่อชีวิต. [YouVersion][]: TNCV, has full audio.

## WBTH ฉบับ อ่านเข้าใจง่าย _(World Bible Translation Center, 2001, now: [International Bible League][])_

**Mark 1:4** - แล้ว​ยอห์น​คน​ทำ​พิธี​จุ่ม​น้ำ มา​ปรากฏ​ตัว​ใน​ที่​เปล่า​เปลี่ยว​แห้ง​แล้ง และ​สั่งสอน​ว่า “ให้​กลับ​ตัว​กลับ​ใจ​เสียใหม่​และ​รับ​พิธี​จุ่ม​น้ำ แล้ว​พระเจ้า​จะ​ยกโทษ​ความ​ผิด​บาป​ของ​พวก​คุณ”

Also called: Easy-to-read New Testament, ERV, ETNT, WBTH (online available on [YouVersion][])
The Old Testament was recently completed in 2016, text freely downloadable at the [International Bible League][]. [YouVersion][]: THA-ERV

## NTV New Thai Translation Version _(๋Jerry & Chareeraat Crow, [YWAM][], 1998)_
**Mark 1:4** - ยอห์นผู้ให้‘บัพติศมาได้ปรากฏตัวในถิ่นทุรกันดาร ประกาศเรื่อง‘บัพติศมา’ซึ่งเกิดจากการกลับใจ เพื่อจะได้รับการยกโทษบาป 

Also called: NTTV, Jerry Crow Bible, ฉบับ แปลใหม่.
A good translation from the original Scriptures trying to avoid royal language and using everyday, modern Thai. The NT is being revised and the OT is almost finished in 2016, with the revision of the OT to be finished in 2017.
Only New Testament in print at the moment (hard cover and vinyl cover), distributed through [Kanok Bannasaan][]. Since the end of 2014 an edition with the Pentateuch, some other OT books and the NT is also available through [Kanok Bannasaan][]. It has been translated from the original languages.

## TKJV Thai King James Version _([Philip Pope][], 2003)_
**Mark 1:4** - ยอห์นให้เขารับบัพติศมาในถิ่นทุรกันดาร และประกาศเรื่องบัพติศมาอันสำแดงการกลับใจใหม่ เพื่อการยกโทษความผิดบาป

Also called: ฉบับ KJV, TKJV (available on [YouVersion][]).
Used in conservative baptist churches. Text available from [Philip Pope][]'s website.
[Crosswire modules][]: ThaiKJV, on [YouVersion][]: KJV, has full audio.

## ThCB Thai Catholic Bible _(Dr.Francis Cais, ผศ.ทัศไนย์คมกฤส)_
**Mark 1:4** - เพื่อให้ข้อความนี้เป็นจริงยอห์นจึงทำพิธีล้างในถิ่นทุรกันดาร เทศน์สอนเรื่องพิธีล้าง ซึ่งแสดงการกลับใจเพื่อจะได้รับการอภัยบาป

There is a module for smartphones: ต้นฉบับภาษาฮีบรูและกรีก ผู้แปล บาทหลวง ดร.ฟรังซิส ไกส์ และ บาทหลวง ผศ.ทัศไนย์ คมกฤส โดย คณะ​กรรม​การคาทอลิกเพื่อคริสตศาสนธรรม แผนกพระคัมภีร์
The full Bible appeared in 2015. The New Testament can be read online on [Issuu ThCB][] and it seems they made some modifications:
**Mark 1:4** - ยอห์นเป็นผู้ทำพิธีล้างในถิ่นทุรกันดาร เทศน์สอนเรื่องพิธีล้าง อันแสดงถึงการกลับใจเพื่อจะได้รับการอภัยบาป
This is available for [MySword][] on Android. It has been translated from the original languages, and has been approved by the Roman Catholic church.

## พระคัมภีร์ ประกอบภาพ
Also called: Thai Cartoon Bible, ฉบับ รวมเล่ม
This is a collection of smaller publications by ไอวา ฮอท, making up a lot of the story sections of the Bible. Distributed through [Kanok Bannasaan][].

## Recovery Version
Also called: ฉบับฟื้นฟู

**Mark 1:4** - โยฮันได้มาให้บัพติสมาในป่ากันดาร และการประกาศเรื่องบัพติสมาแห่งความกลับใจเพื่อความบปาจะได้รับการอภัย

This is the translation by Living Stream Ministry, the organisation of Witness Lee, probably the most literal translation in the Thai (I haven't seen it yet). Ordering information here: [The Lord's Recovery in Thailand][]. Only the New Testament has been translated into Thai yet. All my attempts to order one have failed, the email on their website doesn't work anymore. A number of New Testament books can be listened to at [termsook.com][].
They are using names patterned after the Hebrew pronunciation (มัดธาย for Matthew, โยฮัน for John), and some of the phrases used are different from other translations.

## Good News for Modern Man
I've only heard about this version; apparently it uses more Roman-Catholic Thai words. (I've also heard that Roman-Catholics in Thailand also standardize on the Thai Bible Society's translations, although they have probably now switched to the ThCB.)

## Ann Hasseltine Judson's Gospel of Matthew
'Nancy' Judson was the wife of famous missionary to Burma Adoniram Judson. She was the first Protestant to translate any of the scriptures into Thai when in 1819 she translated the Gospel of Matthew.

## New Buddhist Translation _([Banpote Wetchgama][])_
Aajaan [Banpote Wetchgama][] has started translating books of the New Testament (so far Matthew, John, Romans, Galatians, Ephesians, Philippians, Colossians, 1 & 2 Thessalonians, 1 & 2 Timothy, Hebrews) to be more contextualized for a Thai Buddhist's understanding. Nothing has been published yet.

---

[[Home]](/index) [[About]](/about)

_&copy; 2017 [Peter Passchier][]_
