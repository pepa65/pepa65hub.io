---
title: About
permalink: /about
---

### Interests

All manner of text processing, producing print-ready material, publishing to web.
Getting server applications to work, writing scripts in Bash.
Modifying projects in JavaScript, C and Bash.

### Github repos

#### Started from scratch or major rewrites:

 - [lmdescrypt](https://github.com/pepa65/lmdescrypt)
 - [ezrsync](https://github.com/pepa65/ezrsync)
 - [uvpn](https://github.com/pepa65/uvpn)
 - [thaiworship](https://github.com/pepa65/thaiworship)
 - [misc](https://github.com/pepa65/misc)
 - [deadman](https://github.com/pepa65/deadman)

#### Significant contributions:

 - [didiwiki](https://github.com/pepa65/didiwiki)
 - [weborf](https://github.com/pepa65/weborf)

#### Minor contributions:

 - [silk-guardian](https://github.com/pepa65/silk-guardian)
 - [getcaddy.com](https://github.com/pepa65/getcaddy.com)
 - [dex](https://github.com/pepa65/dex)

[Github contact](mailto:solusos@passchier.net)

[[Home]](/index)
